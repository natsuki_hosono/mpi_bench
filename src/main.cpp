#include "mpi_bench.hpp"

int main(int argc, char** argv){
	const int NLP = 1;
	//MPI_Bench<Chrono_t, double> bench(argc, argv);
	MPI_Bench<MPI_Wtime_t> bench(argc, argv, NLP);
	bench.getNumberOfLocalProcess();
	//MPI_Bench<FJColl_t, double> bench(argc, argv);
	std::cout << "Max allocated memory size " << sizeof(double) * (1ul << 30) / (1024 * 1024 * 1024) << " GiB" << std::endl;
	int rank, size;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	if(0){//All to All
		std::string buffer;
		for(std::size_t i = 1 ; i < (1 << 30) / size / (NLP * NLP) ; i <<= 1){
			auto result = bench.testAllToAll(i);
			//auto result = bench.testAllToAll_BasicLinear(i);
			buffer += std::to_string(result.bytesize) + "\t" + std::to_string(result.wctime) + "\t" + std::to_string(result.throughput) + "\n";
			printf("%lld\t%e\n", result.bytesize, result.wctime);
		}
		std::string filename = "alltoall_" +  std::to_string(rank) + ".txt";
		std::ofstream ofile(filename);
		ofile << buffer << std::endl;
		ofile.close();
	}
	if(0){//All to All V
		std::string buffer;
		for(std::size_t i = 1 ; i < (1 << 30) / size / (NLP * NLP) ; i <<= 1){
			auto result = bench.testAllToAllV(i);
			buffer += std::to_string(result.bytesize) + "\t" + std::to_string(result.wctime) + "\t" + std::to_string(result.throughput) + "\n";
			printf("%lld\t%e\n", result.bytesize, result.wctime);
		}
		std::string filename = "alltoallv_" +  std::to_string(rank) + ".txt";
		std::ofstream ofile(filename);
		ofile << buffer << std::endl;
		ofile.close();
	}
	if(0){//Bcast
		std::string buffer;
		for(std::size_t i = 1 ; i < (1 << 30) / 384 / (NLP) ; i <<= 1){
			const int Ntest = 16;
			double wct_best = 1.0e+30;
			double wct_worst = 0.0;
			double wct_ave = 0.0;
			auto result = bench.testBcast(i);
			const std::size_t bytesize = result.bytesize;
			for(int test = 0 ; test < Ntest ; ++ test){
				result = bench.testBcast(i);
				wct_best = std::min(wct_best, result.wctime);
				wct_worst = std::max(wct_worst, result.wctime);
				wct_ave += result.wctime;
			}
			wct_ave /= Ntest;
			buffer += std::to_string(bytesize) + "\t" + std::to_string(wct_best) + "\t" + std::to_string(wct_ave) + "\t" + std::to_string(wct_worst) + "\n";
		}
		std::string filename = "bcast_" +  std::to_string(rank) + ".txt";
		std::ofstream ofile(filename);
		ofile << buffer << std::endl;
		ofile.close();
	}
	if(0){
		std::string buffer;
		for(std::size_t i = 1 ; i < 1 << 30 ; i <<= 1){
			auto result = bench.testBarrier();
			buffer += std::to_string(result.wctime) + "\n";
		}
		std::string filename = "barrier_" +  std::to_string(rank) + ".txt";
		std::ofstream ofile(filename);
		ofile << buffer << std::endl;
		ofile.close();
	}
	if(0){//test SendRecv
		std::string buffer;
		for(std::size_t i = 1 ; i < 1 << 30 ; i <<= 1){
			auto result = bench.testSendRecv(i);
			buffer += std::to_string(result.bytesize) + "\t" + std::to_string(result.wctime) + "\n";
			printf("%lld\t%e\n", result.bytesize, result.wctime);
		}
		std::string filename = "sendrecv_" +  std::to_string(rank) + ".txt";
		std::ofstream ofile(filename);
		ofile << buffer << std::endl;
		ofile.close();
	}

	if(0){//test PingPong
		std::string buffer;
		for(std::size_t i = 1 ; i < 1 << 30 ; i <<= 1){
			auto result = bench.testPingPong(i);
			buffer += std::to_string(result.bytesize) + "\t" + std::to_string(result.wctime) + "\n";
			printf("%lld\t%e\n", result.bytesize, result.wctime);
		}
		std::string filename = "pingpong_" + std::to_string(rank) + ".txt";
		std::ofstream ofile(filename);
		ofile << buffer << std::endl;
		ofile.close();
	}
	return 0;
}

