#include <mpi.h>
#include <mpi-ext.h>
#include <iostream>
#include <chrono>
#include <fstream>
#include <string>
#include <cstdio>
#include <thread>

class Timer_t{
	public:
	virtual void start() = 0;
	virtual void stop() = 0;
	Timer_t(){
	}
	~Timer_t(){
	}
	virtual double getWCtime() const = 0;
	void test(std::size_t sleeptime){
		//ThisTimer timer;
		start();
		std::this_thread::sleep_for(std::chrono::seconds(sleeptime));
		stop();
		printf("%.16e\n", getWCtime());
	}
};

class MPI_Wtime_t: public Timer_t{
	double begin_, end_;
	public:
	void start(){
		begin_ = MPI_Wtime();
	}
	void stop(){
		end_ = MPI_Wtime();
	}
	double getWCtime() const{
		return end_ - begin_;
	}
};

class Chrono_t: public Timer_t{
	std::chrono::system_clock::time_point begin_, end_;
	public:
	void start(){
		begin_ = std::chrono::high_resolution_clock::now();
	}
	void stop(){
		end_ = std::chrono::high_resolution_clock::now();
	}
	double getWCtime() const{
		return std::chrono::duration_cast<std::chrono::nanoseconds>(end_ - begin_).count() * 1.0e-9;
	}
};

#ifdef FJ_FLAG
class FJColl_t: public Timer_t{
	public:
	void start(){
		FJMPI_Collection_print("timer start");
		FJMPI_Collection_clear();
		FJMPI_Collection_start();
	}
	void stop(){
		FJMPI_Collection_stop();
		FJMPI_Collection_clear();
	}
	double getWCtime() const{
		return -1.0;
	}
};
#endif

template <typename type> MPI_Datatype declMPItype(){
	MPI_Datatype mpidtype;
	MPI_Type_contiguous(sizeof(type), MPI_BYTE, &mpidtype);
	MPI_Type_commit(&mpidtype);
	return mpidtype;
}

template<> inline MPI_Datatype declMPItype<int>(){
	return MPI_INT;
}
template<> inline MPI_Datatype declMPItype<long>(){
	return MPI_LONG;
}
template<> inline MPI_Datatype declMPItype<long long int>(){
	return MPI_LONG_LONG_INT;
}
template<> inline MPI_Datatype declMPItype<unsigned int>(){
	return MPI_UNSIGNED;
}
template<> inline MPI_Datatype declMPItype<unsigned long>(){
	return MPI_UNSIGNED_LONG;
}
template<> inline MPI_Datatype declMPItype<unsigned long long int>(){
	return MPI_UNSIGNED_LONG_LONG;
}
template<> inline MPI_Datatype declMPItype<float>(){
	return MPI_FLOAT;
}
template<> inline MPI_Datatype declMPItype<double>(){
	return MPI_DOUBLE;
}
template<> inline MPI_Datatype declMPItype<long double>(){
	return MPI_LONG_DOUBLE;
}

template <typename MPItype, typename type> MPItype declMPItype(const type x){
	return declMPItype<type>();
}

template <class ThisTimer> class MPI_Bench{
	typedef double real;
	int size, rank;
	static const int abort_code = 1;
	const int NLP;
	public:

	MPI_Bench(const MPI_Bench&) = delete;
	MPI_Bench& operator=(const MPI_Bench&) = delete;
	MPI_Bench(MPI_Bench&&) = delete;
	MPI_Bench& operator=(MPI_Bench&&) = delete;

	struct result_t{
		double wctime;
		std::size_t bytesize;
		double throughput;
	};
	MPI_Bench(int argc, char** argv, const int NLP = 1):NLP(NLP){
		MPI_Init(&argc, &argv);
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		std::cout << "results contains:" << std::endl;
		std::cout << " - bytesize  : total bytesize of allocated array [byte]" << std::endl;
		std::cout << " - wctime    : wall-cloack time [second]" << std::endl;
		std::cout << " - throughput: throughput [byte/second]" << std::endl;
	}
	result_t testAllToAll(const std::size_t send_count = 1) const{
		std::cout << "test All To All" << std::endl;
		real* const send = new real[size * send_count];
		real* const recv = new real[size * send_count];
		std::cout << " - # of procs: " << size << std::endl;
		std::cout << " - # of buffer: " << size * send_count << std::endl;
		std::cout << " - send count: " << send_count << std::endl;
		std::cout << " - bytesize of one element: " << sizeof(real) << " byte" << std::endl;
		std::cout << " -- total size of send/recv buffer: " << sizeof(real) * (size * send_count) / 1e+9 << " GByte" << std::endl;

		for(std::size_t i = 0 ; i < size * send_count ; ++ i){
			send[i] = i / (double)(rank);
			recv[i] = -1.0;
		}
		MPI_Barrier(MPI_COMM_WORLD);
		ThisTimer timer;
		timer.start();
		MPI_Alltoall(send, send_count, MPI_DOUBLE, recv, send_count, MPI_DOUBLE, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		timer.stop();

		result_t result;
		result.bytesize = sizeof(real) * send_count;
		result.wctime = timer.getWCtime();
		result.throughput = 2 * NLP * (size - NLP) * sizeof(real) * send_count / result.wctime;

		delete []recv;
		delete []send;
		std::cout << "end All To All" << std::endl;
		return result;
	}

	result_t testBarrier() const{
		std::cout << "test Barrier" << std::endl;
		result_t result;
		ThisTimer timer;
		timer.start();
		MPI_Barrier(MPI_COMM_WORLD);
		timer.stop();
		result.wctime = timer.getWCtime();
		return result;
	}

	result_t testWtime() const{
		std::cout << "test Wtime" << std::endl;
		result_t result;
		ThisTimer timer;
		timer.start();
		MPI_Wtime();
		timer.stop();
		result.wctime = timer.getWCtime();
		return result;
	}

	void getNumberOfLocalProcess() const{
		MPI_Comm shmcomm;
		MPI_Comm_split_type(MPI_COMM_WORLD, MPI_COMM_TYPE_SHARED, 0, MPI_INFO_NULL, &shmcomm);
		int shmrank;
		MPI_Comm_rank(shmcomm, &shmrank);
		std::cout << shmrank << std::endl;
	}

	void dumpWtime() const{
		std::cout << "dump Wtime" << std::endl;
		{
			ThisTimer timer;
			timer.start();
			timer.stop();
			std::cout << "0\t" << timer.getWCtime() << std::endl;
		}
		{
			ThisTimer timer;
			timer.start();
			timer.stop();
			std::cout << "0\t" << timer.getWCtime() << std::endl;
		}
		{
			ThisTimer timer;
			timer.start();
			MPI_Wtime();
			timer.stop();
			std::cout << "1\t" << timer.getWCtime() << std::endl;
		}
		{
			ThisTimer timer;
			timer.start();
			MPI_Wtime();
			MPI_Wtime();
			timer.stop();
			std::cout << "2\t" << timer.getWCtime() << std::endl;
		}
		{
			ThisTimer timer;
			timer.start();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			timer.stop();
			std::cout << "4\t" << timer.getWCtime() << std::endl;
		}
		{
			ThisTimer timer;
			timer.start();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			timer.stop();
			std::cout << "8\t" << timer.getWCtime() << std::endl;
		}
		{
			ThisTimer timer;
			timer.start();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			MPI_Wtime();
			timer.stop();
			std::cout << "16\t" << timer.getWCtime() << std::endl;
		}
	}

	result_t testBcast(const std::size_t send_count = 1, const int root = 0) const{
		std::cout << "test Bcast" << std::endl;
		real* const buff = new real[send_count];

		for(std::size_t i = 0 ; i < send_count ; ++ i){
			if(rank == root){
				buff[i] = i;
			}else{
				buff[i] = -1.0;
			}
		}

		MPI_Barrier(MPI_COMM_WORLD);
		ThisTimer timer;
		timer.start();
		MPI_Bcast(buff, send_count, MPI_DOUBLE, root, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		timer.stop();

		delete []buff;

		MPI_Info info;
		MPI_Comm_get_info(MPI_COMM_WORLD, &info);
		char mca_value[MPI_MAX_INFO_VAL];
		int flag = 0;
		MPI_Info_get(info, "last_bcast_algorithm", MPI_MAX_INFO_VAL, mca_value, &flag);
		std::cerr << "last_bcast_algorithm = " << mca_value << std::endl;

		result_t result;
		result.wctime = timer.getWCtime();
		result.bytesize = sizeof(real) * send_count;
		result.throughput = sizeof(real) * send_count / result.wctime;
		return result;
	}

	result_t testAllToAllV(const std::size_t send_count = 1) const{
		std::cout << "test All To All V" << std::endl;
		real* const send = new real[size * send_count];
		real* const recv = new real[size * send_count];
		int* const send_counts = new int[size];
		int* const send_displs = new int[size];
		int* const recv_counts = new int[size];
		int* const recv_displs = new int[size];

		std::cout << " - # of procs: " << size << std::endl;
		std::cout << " - # of buffer: " << size * send_count << std::endl;
		std::cout << " - For now, send_counts share the same value: " << std::endl;
		std::cout << " - send count: " << send_count << "x" << size << std::endl;
		std::cout << " - bytesize of one element: " << sizeof(real) << " byte" << std::endl;
		std::cout << " -- total size of send/recv buffer: " << sizeof(real) * (size * send_count) / 1e+9 << " GByte" << std::endl;

		for(std::size_t i = 0 ; i < size * send_count ; ++ i){
			send[i] = i / (double)(rank);
			recv[i] = -1.0;
		}
		for(std::size_t i = 0 ; i < size ; ++ i){
			send_counts[i] = send_count;
			send_displs[i] = i * send_count;
			recv_counts[i] = send_count;
			recv_displs[i] = i * send_count;
		}

		MPI_Barrier(MPI_COMM_WORLD);
		ThisTimer timer;
		timer.start();
		MPI_Alltoallv(send, send_counts, send_displs, MPI_DOUBLE, recv, recv_counts, recv_displs, MPI_DOUBLE, MPI_COMM_WORLD);
		MPI_Barrier(MPI_COMM_WORLD);
		timer.stop();

		MPI_Info info;
		MPI_Comm_get_info(MPI_COMM_WORLD, &info);
		char mca_value[MPI_MAX_INFO_VAL];
		int flag = 0;
		MPI_Info_get(info, "last_alltoallv_algorithm", MPI_MAX_INFO_VAL, mca_value, &flag);
		if(flag){
			std::cerr << "last_alltoallv_algorithm = " << mca_value << std::endl;
		}

		result_t result;
		result.wctime = timer.getWCtime();
		result.bytesize = sizeof(real) * send_count;
		result.throughput = 2 * NLP * (size - NLP) * sizeof(real) * send_count / result.wctime;

		delete []recv;
		delete []send;
		std::cout << "end All To All V" << std::endl;
		return result;
	}
	result_t testPingPong(const std::size_t N) const{
		std::cout << "test Ping Pong" << std::endl;
		result_t result;
		const int src_rank = 0;
		const int dst_rank = 1;
		const int tag = 0;
		MPI_Status stat;
		int ret;

		result.bytesize = sizeof(real) * N;

		if(rank == 0){
			std::cerr << "Rank 0 will send data to Rank 1, then the other way around." << std::endl;
		}
		real* const send = new real[N];
		real* const recv = new real[N];

		srand(rank);
		for(std::size_t i = 0 ; i < N ; ++ i){
			send[i] = rand() / (double)(RAND_MAX);
			recv[i] = -1.0;
		}
		MPI_Barrier(MPI_COMM_WORLD);
		ThisTimer timer;
		timer.start();
		if(rank == src_rank){
			ret = MPI_Send(send, N, MPI_DOUBLE, dst_rank, tag, MPI_COMM_WORLD);
			if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
			ret = MPI_Recv(recv, N, MPI_DOUBLE, dst_rank, tag, MPI_COMM_WORLD, &stat);
			if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
		}else if(rank == dst_rank){
			ret = MPI_Recv(recv, N, MPI_DOUBLE, src_rank, tag, MPI_COMM_WORLD, &stat);
			if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
			ret = MPI_Send(recv, N, MPI_DOUBLE, src_rank, tag, MPI_COMM_WORLD);
			if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
		}
		MPI_Barrier(MPI_COMM_WORLD);
		timer.stop();
		result.wctime = timer.getWCtime();
		//check
		if(rank == src_rank){
			for(std::size_t i = 0 ; i < N ; ++ i){
				if(send[i] != recv[i]) std::cerr << "OOPS! SOMETHING WENT WRONG!" << std::endl;
			}
		}
		
		//fin
		delete []recv;
		delete []send;
		std::cout << "end Ping Pong" << std::endl;
		return result;
	}
	result_t testSendRecv(const std::size_t N) const{
		std::cout << "test Send/Recv" << std::endl;
		result_t result;
		const int tag = 0;
		MPI_Status stat;

		result.bytesize = sizeof(real) * N;

		if(rank == 0){
			std::cerr << "Rank [i] will send data to Rank [i+1] while receiving data from Rank [i-1]" << std::endl;
		}
		real* const send = new real[N];
		real* const recv = new real[N];

		srand(rank);
		for(std::size_t i = 0 ; i < N ; ++ i){
			send[i] = rand() / (double)(RAND_MAX);
			recv[i] = -1.0;
		}
		/*
		int ret;
		if(rank == 0){
			ret = MPI_Send(send, N, PS::GetDataType<real>(), rank + 1, tag, MPI_COMM_WORLD);
			//if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
		}else if(rank == size - 1){
			ret = MPI_Recv(recv, N, PS::GetDataType<real>(), rank - 1, tag, MPI_COMM_WORLD, &stat);
			//if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
		}else{
			MPI_Request send_req, recv_req;
			ret = MPI_Isend(send, N, PS::GetDataType<real>(), rank + 1, tag, MPI_COMM_WORLD, &send_req);
			ret = MPI_Irecv(recv, N, PS::GetDataType<real>(), rank - 1, tag, MPI_COMM_WORLD, &recv_req);
			MPI_Wait(&send_req, &stat);
			MPI_Wait(&recv_req, &stat);
			//if(ret != MPI_SUCCESS) MPI_Abort(MPI_COMM_WORLD, abort_code);
		}
		*/
		const int src_rank = (rank + size - 1) % size;
		const int dst_rank = (rank + 1) % size;
		MPI_Barrier(MPI_COMM_WORLD);
		ThisTimer timer;
		timer.start();
		MPI_Sendrecv(send, N, MPI_DOUBLE, dst_rank, tag, recv, N, MPI_DOUBLE, src_rank, MPI_ANY_TAG, MPI_COMM_WORLD, &stat);
		/*
		MPI_Request send_req, recv_req;
		ret = MPI_Isend(send, N, PS::GetDataType<real>(), dst_rank, tag, MPI_COMM_WORLD, &send_req);
		ret = MPI_Irecv(recv, N, PS::GetDataType<real>(), src_rank, tag, MPI_COMM_WORLD, &recv_req);
		MPI_Wait(&send_req, &stat);
		MPI_Wait(&recv_req, &stat);
		*/
		MPI_Barrier(MPI_COMM_WORLD);
		timer.stop();
		result.wctime = timer.getWCtime();
		delete []recv;
		delete []send;
		std::cout << "end Sned/Recv" << std::endl;
		return result;
	}
	~MPI_Bench(){
		MPI_Finalize();
	}
};

