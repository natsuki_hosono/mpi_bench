#!/bin/sh
#PJM -L  "node=3072:torus"
#PJM -L  "rscgrp=large"
#PJM -L  "elapse=00:01:00"
#PJM --mpi "max-proc-per-node=1"
#PJM -S
#PJM -N "map"

llio_transfer ./mpi_bench.out

mkdir std
mpirun -stdout-proc ./std/stdout -stderr-proc ./std/stderr ./mpi_bench.out
cat ./std/stdout* > coord.txt

