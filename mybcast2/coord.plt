filename = "coord.txt"

reset
set view equal xyz
MaxA = 2
MaxB = 3
MaxC = 2
set ticslevel 0
set size ratio -1

set term pdfcairo enhanced size 8, 8
set out "map.pdf"

set multiplot layout 2, 2
unset key

set xtics 1
set ytics 1
set ztics 1

set xlabel "X(A)"
set ylabel "Y(B)"
set zlabel "Z(C)"
sp filename u (MaxA*$2+$5):(MaxB*$3+$6):(MaxC*$4+$7):8 pt 7 ps 0.25 pal

set xlabel "X(A)"
set ylabel "Y(B)"
p filename u (MaxA*$2+$5):(MaxB*$3+$6):8 pt 7 ps 0.25 pal

set xlabel "Y(B)"
set ylabel "Z(C)"
p filename u (MaxB*$3+$6):(MaxC*$4+$7):8 pt 7 ps 0.25 pal

set xlabel "X(A)"
set ylabel "Z(C)"
p filename u (MaxA*$2+$5):(MaxC*$4+$7):8 pt 7 ps 0.25 pal

reset

