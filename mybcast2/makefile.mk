CPPSRCS = $(wildcard *.cpp)
CPPOBJS = $(patsubst %.cpp, %.o, $(CPPSRCS))
CPPHDRS = $(wildcard *.h)
PROGRAM = mpi_bench.out

.PHONY:	clean all

all:	$(CPPOBJS) $(CPPHDRS)
	@echo "Linking files..."
	@$(CC) $(CFLAGS) $(CPPOBJS) -o $(PROGRAM)
	@echo "Success! [$(PROGRAM)]"

%.o:	%.cpp $(CPPHDRS)
	@echo "Building $< ..."
	@$(CC) -c $< $(CFLAGS)
	@echo "[$< OK]"

clean:
	-rm *.out *.o *.s
