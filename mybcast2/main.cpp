#include "mpi_bench.hpp"
#include <string>

class Topology{
	public:
	int dim, shape_x, shape_y, shape_z;
	int rank, size;
	static const int MaxProcessPerNode = 4;
	static const int MaxA = 2;
	static const int MaxB = 3;
	static const int MaxC = 2;
	// # of physical nodes in each direction
	int NumX = 0;
	int NumY = 0;
	int NumZ = 0;
	// in-unit communicator
	MPI_Comm inUnitComm;
	int inUnitRank, inUnitSize;

	struct tofu_coord_t{
		int X, Y, Z, A, B, C;
	}tofu_coord;
	struct logical_coord_t{
		int X, Y, Z;
	}logical_coord;
	int east_rank[MaxProcessPerNode];
	int west_rank[MaxProcessPerNode];
	int north_rank[MaxProcessPerNode];
	int south_rank[MaxProcessPerNode];
	int up_rank[MaxProcessPerNode];
	int down_rank[MaxProcessPerNode];

	Topology(int argc, char** argv){
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		#ifdef FJ_FLAG
		FJMPI_Topology_get_dimension(&dim);
		FJMPI_Topology_get_shape(&shape_x, &shape_y, &shape_z);
		FJMPI_Topology_get_coords(MPI_COMM_WORLD, rank, FJMPI_LOGICAL , 3, (int*)(&logical_coord));
		//FJMPI_Topology_get_coords(MPI_COMM_WORLD, rank, FJMPI_TOFU_SYS, 6, (int*)(&tofu_coord)   );
		FJMPI_Topology_get_coords(MPI_COMM_WORLD, rank, FJMPI_TOFU_REL, 6, (int*)(&tofu_coord)   );
		int X = MaxA * tofu_coord.X + tofu_coord.A + 1;
		int Y = MaxB * tofu_coord.Y + tofu_coord.B + 1;
		int Z = MaxC * tofu_coord.Z + tofu_coord.C + 1;
		MPI_Allreduce(&X, &NumX, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		MPI_Allreduce(&Y, &NumY, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		MPI_Allreduce(&Z, &NumZ, 1, MPI_INT, MPI_MAX, MPI_COMM_WORLD);
		#else
		dim = -1;
		shape_x = -1;
		shape_y = -1;
		shape_z = -1;
		#endif
		std::cerr << "rank: " << rank << std::endl;
		std::cerr << "size: " << size << std::endl;
		std::cerr << "dimension: " << dim << std::endl;
		std::cerr << "shape: (" << shape_x << ", " << shape_y << ", " << shape_z << ")" << std::endl;
		std::cerr << "coordinate logical (         virtual coord): (" << logical_coord.X << ", " << logical_coord.Y << ", " << logical_coord.Z << ")" << std::endl;
		std::cerr << "coordinate tofu    (shifted physical coord): (" << tofu_coord.X << ", " << tofu_coord.Y << ", " << tofu_coord.Z << ", " << tofu_coord.A << ", " << tofu_coord.B << ", " << tofu_coord.C << ")" << std::endl;
		std::cerr << "physical shape: (" << NumX << ", " << NumY << ", " << NumZ << ")" << std::endl;
		//for visualization
		//std::cout << rank << ", " << logical_coord.X << ", " << logical_coord.Y << ", " << logical_coord.Z << ", " << tofu_coord.X << ", " << tofu_coord.Y << ", " << tofu_coord.Z << ", " << tofu_coord.A << ", " << tofu_coord.B << ", " << tofu_coord.C << std::endl;
		//error
		if(size % 12 != 0){
			DisplayError(1, "THE NUMBER OF PROCESSES MUST BE A MULTIPLE OF 12.");
		}
		if(size != NumX * NumY * NumZ){
			DisplayWarning(2, "INVALID SHAPE OF NODES: TRY AGAIN");
		}
		// create in-unit comm.
		const int clr = tofu_coord.X + NumX * tofu_coord.Y + NumX * NumY * tofu_coord.Z;
		const int key = tofu_coord.A + MaxA * tofu_coord.B + MaxA * MaxB * tofu_coord.C;
		MPI_Comm_split(MPI_COMM_WORLD, clr, key, &inUnitComm);
		MPI_Comm_rank(inUnitComm, &inUnitRank);
		MPI_Comm_size(inUnitComm, &inUnitSize);
		// create inter-unit network
		createInterUnitNetwork();
	}
	int createInterUnitNetwork(){
		int process_per_node;
		int error;

		int east_coord[6] = {tofu_coord.X + 1, tofu_coord.Y, tofu_coord.Z, tofu_coord.A, tofu_coord.B, tofu_coord.C};
		error = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_REL, east_coord, MaxProcessPerNode, &process_per_node, east_rank);
		if(process_per_node != 1 && error == FJMPI_SUCCESS){
			DisplayError(3, "THE NUMBER OF PROCESSES PER NODE MUST BE 1.");
		}
		if(error != FJMPI_SUCCESS){
			east_rank[0] = -1;
		}
		std::cerr << "east rank = " << east_rank[0] << std::endl;

		int west_coord[6] = {tofu_coord.X - 1, tofu_coord.Y, tofu_coord.Z, tofu_coord.A, tofu_coord.B, tofu_coord.C};
		error = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_REL, west_coord, MaxProcessPerNode, &process_per_node, west_rank);
		if(process_per_node != 1 && error == FJMPI_SUCCESS){
			DisplayError(3, "THE NUMBER OF PROCESSES PER NODE MUST BE 1.");
		}
		if(error != FJMPI_SUCCESS){
			west_rank[0] = -1;
		}
		std::cerr << "west rank = " << west_rank[0] << std::endl;

		int north_coord[6] = {tofu_coord.X, tofu_coord.Y + 1, tofu_coord.Z, tofu_coord.A, tofu_coord.B, tofu_coord.C};
		error = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_REL, north_coord, MaxProcessPerNode, &process_per_node, north_rank);
		if(process_per_node != 1 && error == FJMPI_SUCCESS){
			DisplayError(3, "THE NUMBER OF PROCESSES PER NODE MUST BE 1.");
		}
		if(error != FJMPI_SUCCESS){
			north_rank[0] = -1;
		}
		std::cerr << "north rank = " << north_rank[0] << std::endl;

		int south_coord[6] = {tofu_coord.X, tofu_coord.Y - 1, tofu_coord.Z, tofu_coord.A, tofu_coord.B, tofu_coord.C};
		error = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_REL, south_coord, MaxProcessPerNode, &process_per_node, south_rank);
		if(process_per_node != 1 && error == FJMPI_SUCCESS){
			DisplayError(3, "THE NUMBER OF PROCESSES PER NODE MUST BE 1.");
		}
		if(error != FJMPI_SUCCESS){
			south_rank[0] = -1;
		}
		std::cerr << "south rank = " << south_rank[0] << std::endl;

		int up_coord[6] = {tofu_coord.X, tofu_coord.Y, tofu_coord.Z + 1, tofu_coord.A, tofu_coord.B, tofu_coord.C};
		error = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_REL, up_coord, MaxProcessPerNode, &process_per_node, up_rank);
		if(process_per_node != 1 && error == FJMPI_SUCCESS){
			DisplayError(3, "THE NUMBER OF PROCESSES PER NODE MUST BE 1.");
		}
		if(error != FJMPI_SUCCESS){
			up_rank[0] = -1;
		}
		std::cerr << "up rank = " << up_rank[0] << std::endl;

		int down_coord[6] = {tofu_coord.X, tofu_coord.Y, tofu_coord.Z - 1, tofu_coord.A, tofu_coord.B, tofu_coord.C};
		error = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_REL, down_coord, MaxProcessPerNode, &process_per_node, down_rank);
		if(process_per_node != 1 && error == FJMPI_SUCCESS){
			DisplayError(3, "THE NUMBER OF PROCESSES PER NODE MUST BE 1.");
		}
		if(error != FJMPI_SUCCESS){
			down_rank[0] = -1;
		}
		std::cerr << "down rank = " << down_rank[0] << std::endl;
		return 0;
	}
	int Bcast(void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
		DisplayWarning(0, "FOR NOW, THE ROOT RANK IS FIXED TO 0.");
		// (X, Y, Z, 0, 0, 0)
		if(tofu_coord.A == 0 && tofu_coord.B == 0 && tofu_coord.C == 0){
			// 0 -> X-line
			if(NumX != MaxA){
				if(tofu_coord.X == 0 && tofu_coord.Y == 0 && tofu_coord.Z == 0){
					MPI_Send(buf, count, datatype, east_rank[0], 0, comm);
				}else if(east_rank[0] == -1 && tofu_coord.Y == 0 && tofu_coord.Z == 0){
					MPI_Recv(buf, count, datatype, west_rank[0], 0, comm, nullptr);
				}else if(tofu_coord.Y == 0 && tofu_coord.Z == 0){
					MPI_Recv(buf, count, datatype, west_rank[0], 0, comm, nullptr);
					MPI_Send(buf, count, datatype, east_rank[0], 0, comm);
				}
			}
			// X-line -> XY-plane
			if(NumY != MaxB){
				if(tofu_coord.Y == 0 && tofu_coord.Z == 0){
					MPI_Send(buf, count, datatype, north_rank[0], 0, comm);
				}else if(north_rank[0] == -1 && tofu_coord.Z == 0){
					MPI_Recv(buf, count, datatype, south_rank[0], 0, comm, nullptr);
				}else if(tofu_coord.Z == 0){
					MPI_Recv(buf, count, datatype, south_rank[0], 0, comm, nullptr);
					MPI_Send(buf, count, datatype, north_rank[0], 0, comm);
				}
			}
			// XY-plane -> XYZ cube(?)
			if(NumZ != MaxC){
				if(tofu_coord.Z == 0){
					MPI_Send(buf, count, datatype, up_rank[0], 0, comm);
				}else if(up_rank[0] == -1){
					MPI_Recv(buf, count, datatype, down_rank[0], 0, comm, nullptr);
				}else{
					MPI_Recv(buf, count, datatype, down_rank[0], 0, comm, nullptr);
					MPI_Send(buf, count, datatype, up_rank[0], 0, comm);
				}
			}
		}
		IntraUnitBcast(buf, count, datatype, 0, inUnitComm);
		return 0;
	}
	int Bcast2(void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
		DisplayWarning(0, "FOR NOW, THE ROOT RANK IS FIXED TO 0.");
		IntraUnitBcast(buf, count, datatype, 0, inUnitComm);
		// 0 -> X-line
		if(NumX != MaxA){
			if(tofu_coord.X == 0 && tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Send(buf, count, datatype, east_rank[0], 0, comm);
			}else if(east_rank[0] == -1 && tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, west_rank[0], 0, comm, nullptr);
			}else if(tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, west_rank[0], 0, comm, nullptr);
				MPI_Send(buf, count, datatype, east_rank[0], 0, comm);
			}
		}
		// X-line -> XY-plane
		if(NumY != MaxB){
			if(tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Send(buf, count, datatype, north_rank[0], 0, comm);
			}else if(north_rank[0] == -1 && tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, south_rank[0], 0, comm, nullptr);
			}else if(tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, south_rank[0], 0, comm, nullptr);
				MPI_Send(buf, count, datatype, north_rank[0], 0, comm);
			}
		}
		// XY-plane -> XYZ cube(?)
		if(NumZ != MaxC){
			if(tofu_coord.Z == 0){
				MPI_Send(buf, count, datatype, up_rank[0], 0, comm);
			}else if(up_rank[0] == -1){
				MPI_Recv(buf, count, datatype, down_rank[0], 0, comm, nullptr);
			}else{
				MPI_Recv(buf, count, datatype, down_rank[0], 0, comm, nullptr);
				MPI_Send(buf, count, datatype, up_rank[0], 0, comm);
			}
		}
		return 0;
	}
	int IntraInterBcast(void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
		DisplayWarning(0, "FOR NOW, THE ROOT RANK IS FIXED TO 0.");
		IntraUnitBcast(buf, count, datatype, 0, inUnitComm);
		InterUnitBcast(buf, count, datatype, 0, MPI_COMM_WORLD);
		return 0;
	}
	int InterIntraBcast(void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
		DisplayWarning(0, "FOR NOW, THE ROOT RANK IS FIXED TO 0.");
		if(tofu_coord.A == 0 && tofu_coord.B == 0 && tofu_coord.C == 0){
			InterUnitBcast(buf, count, datatype, 0, MPI_COMM_WORLD);
		}
		IntraUnitBcast(buf, count, datatype, 0, inUnitComm);
		return 0;
	}
	int InterUnitBcast(void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
		// 0 -> X-line
		if(NumX != MaxA){
			if(tofu_coord.X == 0 && tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Send(buf, count, datatype, east_rank[0], 0, comm);
			}else if(east_rank[0] == -1 && tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, west_rank[0], 0, comm, nullptr);
			}else if(tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, west_rank[0], 0, comm, nullptr);
				MPI_Send(buf, count, datatype, east_rank[0], 0, comm);
			}
		}
		// X-line -> XY-plane
		if(NumY != MaxB){
			if(tofu_coord.Y == 0 && tofu_coord.Z == 0){
				MPI_Send(buf, count, datatype, north_rank[0], 0, comm);
			}else if(north_rank[0] == -1 && tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, south_rank[0], 0, comm, nullptr);
			}else if(tofu_coord.Z == 0){
				MPI_Recv(buf, count, datatype, south_rank[0], 0, comm, nullptr);
				MPI_Send(buf, count, datatype, north_rank[0], 0, comm);
			}
		}
		// XY-plane -> XYZ cube(?)
		if(NumZ != MaxC){
			if(tofu_coord.Z == 0){
				MPI_Send(buf, count, datatype, up_rank[0], 0, comm);
			}else if(up_rank[0] == -1){
				MPI_Recv(buf, count, datatype, down_rank[0], 0, comm, nullptr);
			}else{
				MPI_Recv(buf, count, datatype, down_rank[0], 0, comm, nullptr);
				MPI_Send(buf, count, datatype, up_rank[0], 0, comm);
			}
		}
		return 0;
	}
	int IntraUnitBcast(void *buf, int count, MPI_Datatype datatype, int root, MPI_Comm comm){
		#if 1
		MPI_Bcast(buf, count, datatype, 0, inUnitComm);
		#else
		if(count < inUnitSize){
			MPI_Bcast(buf, count, datatype, 0, inUnitComm);
		}else{
			int* sendcount = new int[inUnitSize];
			int* senddispls = new int[inUnitSize];
			int displs = 0;
			for(int i = 0 ; i < inUnitSize - 1 ; ++ i){
				sendcount[i] = count / inUnitSize;
				senddispls[i] = displs;
				displs += sendcount[i];
			}
			sendcount[inUnitSize - 1] = count - displs;
			senddispls[inUnitSize - 1] = displs;
			MPI_Scatterv(buf, sendcount, senddispls, datatype, buf, sendcount[inUnitRank], datatype, 0, inUnitComm);
			MPI_Allgatherv(buf, sendcount[inUnitRank], datatype, buf, sendcount, senddispls, datatype, inUnitComm);
			delete[] sendcount;
			delete[] senddispls;
		}
		#endif
		return 0;
	}
	~Topology(){
	}
	void DisplayError(const int errorcode, const std::string str) const{
		std::cerr << "ERROR! (code " << errorcode << ") " << str << std::endl;
		MPI_Abort(MPI_COMM_WORLD, errorcode);
	}
	void DisplayWarning(const int errorcode, const std::string str) const{
		std::cerr << "WARNING! (code " << errorcode << ") " << str << std::endl;
	}
};

int main(int argc, char** argv){
	MPI_Init(&argc, &argv);
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	Topology tplgy(argc, argv);
	#if 1
	const int N = 12*128;
	double* buf = new double[N];
	if(rank == 0){
		for(int i = 0 ; i < N ; ++ i){
			buf[i] = 1.5 + i;
		}
	}else{
		for(int i = 0 ; i < N ; ++ i){
			buf[i] = -1.0;
		}
	}
	//tplgy.InterIntraBcast(buf, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);
	//tplgy.IntraInterBcast(buf, N, MPI_DOUBLE, 0, MPI_COMM_WORLD);

	int tofu[6];
	FJMPI_Topology_get_coords(MPI_COMM_WORLD, rank, FJMPI_TOFU_REL, 6, tofu);
	std::cout << rank << " " << tofu[0] << " " << tofu[1] << " " << tofu[2] << " " << tofu[3] << " " << tofu[4] << " " << tofu[5] << " " << buf[0] << " " << buf[7] << std::endl;
	for(int i = 0 ; i < N ; ++ i){
		std::cout << "# " << buf[i] << std::endl;
	}
	#else
	const int N = 1 << 20;
	double* buf = new double[N];
	if(rank == 0){
		for(int i = 0 ; i < N ; ++ i){
			buf[i] = 1.5 + i;
		}
	}else{
		for(int i = 0 ; i < N ; ++ i){
			buf[i] = -1.0;
		}
	}
	for(int send_cnt = 1 ; send_cnt <= N ; send_cnt *= 2){
		double wtime_mpi = 1.0e+30;
		double wtime_my = 1.0e+30;
		double wtime_my2 = 1.0e+30;
		for(int i = 0 ; i < 16 ; ++ i){
			double begin_mpi = MPI_Wtime();
			MPI_Bcast(buf, send_cnt, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			double end_mpi = MPI_Wtime();
			wtime_mpi = std::min(end_mpi - begin_mpi, wtime_mpi);

			double begin_my = MPI_Wtime();
			tplgy.InterIntraBcast(buf, send_cnt, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			double end_my = MPI_Wtime();
			wtime_my = std::min(end_my - begin_my, wtime_my);

			double begin_my2 = MPI_Wtime();
			tplgy.IntraInterBcast(buf, send_cnt, MPI_DOUBLE, 0, MPI_COMM_WORLD);
			MPI_Barrier(MPI_COMM_WORLD);
			double end_my2 = MPI_Wtime();
			wtime_my2 = std::min(end_my2 - begin_my2, wtime_my2);
		}
		std::cout << send_cnt * sizeof(double) << "\t" << wtime_mpi << "\t" << wtime_my << "\t" << wtime_my2 << std::endl;
	}
	#endif

	MPI_Finalize();
}

