#!/bin/sh
#PJM -L  "node=1x1x1"
#PJM -L  "rscgrp=eap-small"
#PJM -L  "elapse=00:01:00"
#PJM --mpi "max-proc-per-node=4"
#PJM -S

mpirun -stdout-proc ./stdout -stderr-proc ./stderr -n 4 ./a.out

