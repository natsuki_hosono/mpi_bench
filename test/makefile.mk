PS_PATH = -I ../FDPS/src/

CPPSRCS = $(wildcard *.cpp)
CPPOBJS = $(patsubst %.cpp, %.o, $(CPPSRCS))
CPPHDRS = $(wildcard *.h)
PROGRAM = a.out

.PHONY:	clean all

all:	$(CPPOBJS) $(CPPHDRS)
	@echo "Linking files..."
	@$(CC) $(CFLAGS) $(CPPOBJS) -o $(PROGRAM)
	@echo "Success! [$(PROGRAM)]"

%.o:	%.cpp $(CPPHDRS)
	@echo "Building $< ..."
	@$(CC) -c $< $(CFLAGS) $(PS_PATH)
	@echo "[$< OK]"

clean:
	-rm *.out *.o *.lst *.s
