#include <mpi.h>
#include <omp.h>
#include <mpi-ext.h>
#include <iostream>
#include <chrono>

class Timer_t{
	public:
	virtual void start() = 0;
	virtual void stop() = 0;
	Timer_t(){
	}
	~Timer_t(){
	}
	virtual double getWCtime() const = 0;
};

class MPI_Wtime_t: public Timer_t{
	double begin_, end_;
	public:
	void start(){
		begin_ = MPI_Wtime();
	}
	void stop(){
		end_ = MPI_Wtime();
	}
	double getWCtime() const{
		return end_ - begin_;
	}
};

class Chrono_t: public Timer_t{
	std::chrono::system_clock::time_point begin_, end_;
	public:
	void start(){
		begin_ = std::chrono::high_resolution_clock::now();
	}
	void stop(){
		end_ = std::chrono::high_resolution_clock::now();
	}
	double getWCtime() const{
		return std::chrono::duration_cast<std::chrono::nanoseconds>(end_ - begin_).count() * 1.0e-9;
	}
};

class Topology{
	public:
	int dim_size, x, y, z;
	int rank, size;
	//int coords_logical[3];//X Y Z
	static const int MaxProcessPerNode = 4;
	static const int MaxA = 2;
	static const int MaxB = 3;
	static const int MaxC = 2;
	struct tofu_coord_t{
		int X, Y, Z, A, B, C;
	}tofu_coord;
	struct result_t{
		double wctime;
		std::size_t bytesize;
	};
	Topology(){
		MPI_Comm_rank(MPI_COMM_WORLD, &rank);
		MPI_Comm_size(MPI_COMM_WORLD, &size);
		#ifdef FJ_FLAG
		FJMPI_Topology_get_dimension(&dim_size);
		FJMPI_Topology_get_shape(&x, &y, &z);
		//FJMPI_Topology_get_coords(MPI_COMM_WORLD, rank, FJMPI_LOGICAL, 3, coords_logical);
		FJMPI_Topology_get_coords(MPI_COMM_WORLD, rank, FJMPI_TOFU_SYS, 6, (int*)(&tofu_coord));
		#else
		dim_size = -1;
		x = -1;
		y = -1;
		z = -1;
		#endif
		std::cerr << "rank: " << rank << std::endl;
		std::cerr << "size: " << size << std::endl;
		std::cerr << "dimension: " << dim_size << std::endl;
		std::cerr << "shape: (" << x << ", " << y << ", " << z << ")" << std::endl;
		//std::cerr << "coordinate logical: (" << coords_logical[0] << ", " << coords_logical[1] << ", " << coords_logical[2] << ")" << std::endl;
		std::cerr << "coordinate tofu: (" << tofu_coord.X << ", " << tofu_coord.Y << ", " << tofu_coord.Z << ", " << tofu_coord.A << ", " << tofu_coord.B << ", " << tofu_coord.C << ")" << std::endl;
	}
	void getRanksInTheSameNode(int* ranks, int* process_per_node) const{
		//int outppn;//returned process-per-node
		FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_SYS, (int*)(&tofu_coord), MaxProcessPerNode, process_per_node, ranks);
	}
	void getNeighbourRanks(int* ranks, int* ngb_size) const{
		*ngb_size = 0;
		for(int a = 0 ; a < MaxA ; ++ a){
			for(int b = 0 ; b < MaxB ; ++ b){
				for(int c = 0 ; c < MaxC ; ++ c){
					const tofu_coord_t ngb_coord = tofu_coord_t{tofu_coord.X, tofu_coord.Y, tofu_coord.Z, a, b, c};
					int outppn;
					int target_ranks[MaxProcessPerNode];
					const int err = FJMPI_Topology_get_ranks(MPI_COMM_WORLD, FJMPI_TOFU_SYS, (int*)(&ngb_coord), MaxProcessPerNode, &outppn, target_ranks);
					if(err == FJMPI_ERR_TOPOLOGY_NO_PROCESS) continue;
					for(int rank = 0 ; rank < outppn ; ++ rank){
						ranks[(*ngb_size) ++] = target_ranks[rank];
					}
				}
			}
		}
	}
	template <class ThisTimer> result_t testIsendIrecv(const std::size_t N) const{
		result_t result;
		result.bytesize = sizeof(double) * N;
		int ngb_ranks[MaxProcessPerNode * MaxA * MaxB * MaxC];
		int ngb_size;
		getNeighbourRanks(ngb_ranks, &ngb_size);
		double* const send = new double[N];
		double* const recv = new double[N * ngb_size];
		MPI_Request send_req[MaxProcessPerNode * MaxA * MaxB * MaxC], recv_req[MaxProcessPerNode * MaxA * MaxB * MaxC];
		MPI_Status send_stat[MaxProcessPerNode * MaxA * MaxB * MaxC], recv_stat[MaxProcessPerNode * MaxA * MaxB * MaxC];
		MPI_Barrier(MPI_COMM_WORLD);
		ThisTimer timer;
		timer.start();
		for(int r = 0 ; r < ngb_size ; ++ r){
			const int tag = 0;
			MPI_Isend(  send        , N, MPI_DOUBLE, r, tag, MPI_COMM_WORLD, &(send_req[r]));
			MPI_Irecv(&(recv[r * N]), N, MPI_DOUBLE, r, tag, MPI_COMM_WORLD, &(recv_req[r]));
		}
		MPI_Waitall(ngb_size, send_req, send_stat);
		MPI_Waitall(ngb_size, recv_req, recv_stat);
		timer.stop();
		MPI_Barrier(MPI_COMM_WORLD);
		result.wctime = timer.getWCtime();
		delete [] send;
		delete [] recv;
		return result;
	}
};

int main(int argc, char** argv){
	MPI_Init(&argc, &argv);
	Topology tplgy;
	/*
	if(tplgy.rank == 0){
		std::cout << "ranks in the same node" << std::endl;
		int ranks[tplgy.MaxProcessPerNode];
		int ppn;//process-per-node
		tplgy.getRanksInTheSameNode(ranks, &ppn);
		for(int r = 0 ; r < ppn ; ++ r){
			std::cout << ranks[r] << std::endl;
		}
		std::cout << "ranks in the same group (neighbour)" << std::endl;
		int ngb_ranks[tplgy.MaxProcessPerNode * tplgy.MaxA * tplgy.MaxB * tplgy.MaxC];
		int ngb_size;
		tplgy.getNeighbourRanks(ngb_ranks, &ngb_size);
		for(int r = 0 ; r < ngb_size ; ++ r){
			std::cout << ngb_ranks[r] << std::endl;
		}
	}
	*/
	Topology::result_t result = tplgy.testIsendIrecv<MPI_Wtime_t>(1);
	for(std::size_t N = 1 ; N < (1u << 21) ; N *= 2){
		Topology::result_t result = tplgy.testIsendIrecv<MPI_Wtime_t>(N);
		std::cout << result.bytesize << " " << result.wctime << std::endl;
	}
	
	//Topology::result_t result = tplgy.testIsendIrecv<MPI_Wtime_t>(1);
	MPI_Finalize();
	return 0;
}

